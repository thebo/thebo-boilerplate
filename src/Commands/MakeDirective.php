<?php

namespace KobaltDigital\Commands;

use KobaltDigital\StubGenerator;

class MakeDirective extends StubGenerator
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'make:directive {name} {--force}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Adds a new blade directive.';

    /**
     * Get the stub files for the generator.
     *
     * @return array[]
     */
    protected function getStubs(): array
    {
        return [
          'App/Directives' => [
             'stub' => dirname(__FILE__, 2) . '/stubs/directive.stub',
             'extension' => '.php',
             'type' => 'Directive'
          ],
        ];
    }

    /**
     * Runs after the command is finished
     *
     * @return void
     */
    protected function afterCreate(): void
    {
        delete_transient('user/directives');
    }
}
