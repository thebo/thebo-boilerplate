<?php

namespace KobaltDigital\Commands;

use Illuminate\Console\Command;

class ViewClear extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'view:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear all compiled view files';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        array_map('unlink', array_filter((array) glob(sprintf("%s/*", config('blade.cache')))));
        $this->info('Compiled views cleared!');
    }
}
