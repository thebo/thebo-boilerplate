<?php

namespace KobaltDigital\Commands;

use Illuminate\Support\Str;
use KobaltDigital\StubGenerator;

class MakeAction extends StubGenerator
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'make:action {name} {--action=} {--force}';


    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Adds a callback function to an action hook.';

    /**
     * Define custom variables to replace in the stubs.
     *
     * @return array
     */
    public function getVariables(): array
    {
        $actions = getActions();

        $action = $this->getPredefinedActionName($actions);

        if (array_key_exists($action, $actions)) {
            $action = $actions[$action];
        }

        if (is_array($action)) {
            return [
                'action' => $action['action'],
                'parameters' => collect($action['parameters'])->pluck('name')->implode(', '),
                'documentation' => $action['docblock']
            ];
        }

        return [
            'action' => $action,
            'parameters' => '',
            'documentation' => $this->getDefaultDocumentation(),
        ];
    }

    private function getDefaultDocumentation(): string
    {
        return '    /**
     * The callback to be run when the action is called.
     *
     * @return void
     */';
    }

    private function getPredefinedActionName($actions)
    {
        $name = trim($this->argument('name'));

        if ($this->option('action')) {
            return $this->option('action');
        }

        if (Str::contains($name, 'On')) {
            return Str::snake(Str::afterLast($name, 'On'));
        }

        if (Str::contains($name, 'When') && Str::contains($name, 'IsCalled')) {
            return Str::snake(Str::between($name, 'When', 'IsCalled'));
        }

        if (Str::contains($name, 'When') && Str::contains($name, 'IsCalled')) {
            return Str::snake(Str::between($name, 'When', 'IsCalled'));
        }

        return $this->anticipate('what is the name of the action?', array_keys($actions));
    }

    /**
     * Get the stub files for the generator.
     *
     * @return array[]
     */
    protected function getStubs(): array
    {
        return [
            'App/Actions' => [
                'stub' => dirname(__FILE__, 2) . '/stubs/action.stub',
                'extension' => '.php',
                'type' => 'Action'
            ],
        ];
    }

    /**
     * Runs after the command is finished
     *
     * @return void
     */
    protected function afterCreate(): void
    {
        delete_transient('user/actions');
    }
}
