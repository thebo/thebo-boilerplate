<?php

namespace KobaltDigital\Commands;

use KobaltDigital\StubGenerator;

class MakeCommand extends StubGenerator
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'make:command {name} {--force}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new Artisan command';

    /**
     * Get the stub files for the generator.
     *
     * @return array[]
     */
    protected function getStubs(): array
    {
        return [
          'App/Commands' => [
              'stub' => dirname(__FILE__, 2) . '/stubs/console.stub',
              'extension' => '.php',
              'type' => 'Command'
            ],
        ];
    }

    /**
     * Runs after the command is finished
     *
     * @return void
     */
    protected function afterCreate(): void
    {
        delete_transient('user/commands');
    }
}
