<?php

namespace KobaltDigital\Commands;

use Illuminate\Support\Str;
use KobaltDigital\StubGenerator;

class MakeFilter extends StubGenerator
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'make:filter {name} {--filter=} {--force}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Adds a callback function to a filter hook.';

    /**
     * Define custom variables to replace in the stubs.
     *
     * @return array
     */
    public function getVariables(): array
    {
        $filters = getFilters();

        $filter = $this->getPredefinedFilterName($filters);

        if (array_key_exists($filter, $filters)) {
            $filter = $filters[$filter];
        }

        if (is_array($filter)) {
            return [
                'filter' => $filter['filter'],
                'parameters' => collect($filter['parameters'])->pluck('name')->implode(', '),
                'documentation' => $filter['docblock'] !== "" ? $filter['docblock'] : $this->getDefaultDocumentation(),
                'return' =>  isset($filter['parameters'][0]) ? $filter['parameters'][0]['name'] : 0
            ];
        }

        return [
            'filter' => $filter,
            'parameters' => '',
            'documentation' => $this->getDefaultDocumentation(),
            'return' => '0',
        ];
    }

    private function getDefaultDocumentation(): string
    {
        return '    /**
     * The callback to be run when the filter is applied.
     *
     * @return any
     */';
    }

    private function getPredefinedFilterName($filters)
    {
        $name = trim($this->argument('name'));

        if ($this->option('filter')) {
            return $this->option('filter');
        }

        if (Str::contains($name, 'On')) {
            return Str::snake(Str::afterLast($name, 'On'));
        }

        if (Str::contains($name, 'When') && Str::contains($name, 'IsApplied')) {
            return Str::snake(Str::between($name, 'When', 'IsApplied'));
        }

        return $this->anticipate('what is the name of the filter?', array_keys($filters));
    }

    /**
     * Get the stub files for the generator.
     *
     * @return array[]
     */
    protected function getStubs(): array
    {
        return [
            'App/Filters' => [
                'stub' => dirname(__FILE__, 2) . '/stubs/filter.stub',
                'extension' => '.php',
                'type' => 'Filter'
            ],
        ];
    }

    /**
     * Runs after the command is finished
     *
     * @return void
     */
    protected function afterCreate(): void
    {
        delete_transient('user/filters');
    }
}
