<?php

namespace KobaltDigital\Commands;

use Illuminate\Support\Str;
use KobaltDigital\StubGenerator;

class MakeComponent extends StubGenerator
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'make:component {name} {--force}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new view component class';

    /**
     * Define custom variables to replace in the stubs.
     *
     * @return array
     */
    public function getVariables(): array
    {
        return [
          'view' => str_replace('/', '.', Str::lower(trim($this->argument('name'))))
        ];
    }

    /**
     * Get the stub files for the generator.
     *
     * @return array[]
     */
    protected function getStubs(): array
    {
        return [
          'App/Components' => [
            'stub' => dirname(__FILE__, 2) . '/stubs/component-class.stub',
            'extension' => '.php',
            'type' => 'Component Class'
          ],
          'resources/views/components' => [
            'stub' => dirname(__FILE__, 2) . '/stubs/component-view.stub',
            'extension' => '.blade.php',
            'type' => 'Component View'
          ],
        ];
    }

    /**
     * Runs after the command is finished
     *
     * @return void
     */
    protected function afterCreate(): void
    {
        delete_transient('user/components');
    }
}
