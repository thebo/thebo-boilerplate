<?php

namespace KobaltDigital\Commands;

use Illuminate\Support\Str;
use KobaltDigital\StubGenerator;

class MakePostType extends StubGenerator
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'make:post-type {name} {--force} {--full}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new Custom Post Type';

    /**
     * Define custom variables to replace in the stubs.
     *
     * @return array
     */
    protected function getVariables(): array
    {
        $name = trim($this->argument('name'));

        return [
          'single' => Str::lower(Str::singular($name)),
          'usingle' => Str::ucfirst(Str::singular($name)),
          'plural' => Str::lower(Str::plural($name)),
          'uplural' => Str::ucfirst(Str::plural($name)),
        ];
    }

    /**
     * Get the stub files for the generator.
     *
     * @return array[]
     */
    protected function getStubs(): array
    {
        if ($this->option('full')) {
            return [
              'App/PostTypes' => [
                  'stub' => dirname(__FILE__, 2) . '/stubs/post-type.stub',
                  'extension' => '.php',
                  'type' => 'Post Type'
                ],
            ];
        }

        return [
            'App/PostTypes' => [
              'stub' => dirname(__FILE__, 2) . '/stubs/post-type-simple.stub',
              'extension' => '.php',
              'type' => 'Post Type'
            ],
        ];
    }

    /**
     * Runs after the command is finished
     *
     * @return void
     */
    protected function afterCreate(): void
    {
        delete_transient('user/post_types');
    }
}
