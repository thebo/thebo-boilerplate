<?php

namespace KobaltDigital\Commands;

use Illuminate\Support\Str;
use KobaltDigital\StubGenerator;

class MakeSidebar extends StubGenerator
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'make:sidebar {name} {--force} {--full}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new sidebar';

    /**
     * Define custom variables to replace in the stubs.
     *
     * @return array
     */
    protected function getVariables(): array
    {
        $name = trim($this->argument('name'));

        return [
          'name' => Str::headline($name),
          'id' => Str::slug($name),
        ];
    }

    /**
     * Get the stub files for the generator.
     *
     * @return array[]
     */
    protected function getStubs(): array
    {
        if ($this->option('full')) {
            return [
              'App/Sidebars' => [
                  'stub' => dirname(__FILE__, 2) . '/stubs/sidebar.stub',
                  'extension' => '.php',
                  'type' => 'Sidebar'
                ],
            ];
        }

        return [
            'App/Sidebars' => [
              'stub' => dirname(__FILE__, 2) . '/stubs/sidebar-simple.stub',
              'extension' => '.php',
              'type' => 'Sidebar'
            ],
        ];
    }

    /**
     * Runs after the command is finished
     *
     * @return void
     */
    protected function afterCreate(): void
    {
        delete_transient('user/sidebars');
    }
}
