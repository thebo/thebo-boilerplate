<?php

namespace KobaltDigital\Commands;

use KobaltDigital\StubGenerator;

class MakeShortcode extends StubGenerator
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'make:shortcode {name} {--force}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Adds a new shortcode.';

    /**
     * Get the stub files for the generator.
     *
     * @return array[]
     */
    protected function getStubs(): array
    {
        return [
          'App/Shortcodes' => [
              'stub' => dirname(__FILE__, 2) . '/stubs/shortcode.stub',
              'extension' => '.php',
              'type' => 'Shortcode'
            ],
        ];
    }

    /**
     * Runs after the command is finished
     *
     * @return void
     */
    protected function afterCreate(): void
    {
        delete_transient('user/shortcodes');
    }
}
