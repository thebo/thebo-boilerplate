<?php

namespace KobaltDigital\Commands;

use Illuminate\Console\Command;

class CacheClear extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cache:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear all transients';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        global $wpdb;

        $sql = 'DELETE FROM ' . $wpdb->options . ' WHERE option_name LIKE "_transient_%"';
        $wpdb->query($sql);

        $this->info('Cache cleared!');
    }
}
