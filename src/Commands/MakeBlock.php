<?php

namespace KobaltDigital\Commands;

use Illuminate\Support\Str;
use KobaltDigital\StubGenerator;

class MakeBlock extends StubGenerator
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'make:block {name} {--force}';


    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Adds a new ACF block.';


    /**
     * Define custom variables to replace in the stubs.
     *
     * @return array
     */
    public function getVariables(): array
    {
        return [
            'view' => str_replace('/', '.', Str::lower(trim($this->argument('name')))),
            'title' => Str::headline(trim($this->argument('name'))),
            'name' => Str::slug(trim($this->argument('name'))),
        ];
    }

    /**
     * Get the stub files for the generator.
     *
     * @return array[]
     */
    protected function getStubs(): array
    {
        return [
          'App/Acf/Blocks' => [
              'stub' => dirname(__FILE__, 2) . '/stubs/block-class.stub',
              'extension' => '.php',
              'type' => 'Block Class'
          ],
            'resources/views/blocks' => [
                'stub' => dirname(__FILE__, 2) . '/stubs/block-view.stub',
                'extension' => '.blade.php',
                'type' => 'Block View'
            ],
        ];
    }

    /**
     * Runs after the command is finished
     *
     * @return void
     */
    protected function afterCreate(): void
    {
        delete_transient('user/blocks');
        if (!function_exists('acf_register_block_type')) {
            $this->warn('ACF plugin is not installed or activated.');
        }
    }
}
