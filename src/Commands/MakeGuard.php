<?php

namespace KobaltDigital\Commands;

use KobaltDigital\StubGenerator;

class MakeGuard extends StubGenerator
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'make:guard {name} {--force}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Adds a new blade guard.';

    /**
     * Get the stub files for the generator.
     *
     * @return array[]
     */
    protected function getStubs(): array
    {
        return [
            'App/Guards' => [
              'stub' => dirname(__FILE__, 2) . '/stubs/guard.stub',
              'extension' => '.php',
              'type' => 'Guard'
            ],
        ];
    }

    /**
     * Runs after the command is finished
     *
     * @return void
     */
    protected function afterCreate(): void
    {
        delete_transient('user/guards');
    }
}
