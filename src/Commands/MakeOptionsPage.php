<?php

namespace KobaltDigital\Commands;

use Illuminate\Support\Str;
use KobaltDigital\StubGenerator;

class MakeOptionsPage extends StubGenerator
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'make:options-page {name} {--force}';


    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Adds a new ACF option page.';


    /**
     * Define custom variables to replace in the stubs.
     *
     * @return array
     */
    public function getVariables(): array
    {
        return [
            'title' => Str::headline(trim($this->argument('name'))),
            'slug' => Str::slug(trim($this->argument('name'))),
        ];
    }

    /**
     * Get the stub files for the generator.
     *
     * @return array[]
     */
    protected function getStubs(): array
    {
        return [
          'App/Acf/OptionsPages' => [
              'stub' => dirname(__FILE__, 2) . '/stubs/options-page.stub',
              'extension' => '.php',
              'type' => 'Options page'
          ],
        ];
    }

    /**
     * Runs after the command is finished
     *
     * @return void
     */
    protected function afterCreate(): void
    {
        delete_transient('user/options-pages');
        if (!function_exists('acf_register_block_type')) {
            $this->warn('ACF plugin is not installed or activated.');
        }
    }
}
