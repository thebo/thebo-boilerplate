<?php

namespace KobaltDigital\Guards;

use KobaltDigital\Extend\Guard;

class Auth extends Guard
{
    /**
     * Guard name to be replaced in blade files.
     *
     * @var string
     */
    protected string $guard = 'auth';

    /**
     * The callback function to call when the guard is run.
     *
     * @return boolean
     */
    public function handle(): bool
    {
        return is_user_logged_in();
    }
}
