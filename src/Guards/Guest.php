<?php

namespace KobaltDigital\Guards;

use KobaltDigital\Extend\Guard;

class Guest extends Guard
{
    /**
     * Guard name to be replaced in blade files.
     *
     * @var string
     */
    protected string $guard = 'guest';

    /**
     * The callback function to call when the guard is run.
     *
     * @return boolean
     */
    public function handle(): bool
    {
        return !is_user_logged_in();
    }
}
