<?php

namespace KobaltDigital\Guards;

use KobaltDigital\Extend\Guard;

class IsNot extends Guard
{
    /**
     * Guard name to be replaced in blade files.
     *
     * @var string
     */
    protected string $guard = 'isnot';

    /**
     * The callback function to call when the guard is run.
     *
     * @param $value
     * @return bool
     */
    public function handle($value): bool
    {
        $arguments = func_get_args();
        $method = array_shift($arguments);
        $method = sprintf('is_%s', strtolower($method));

        if (!is_callable($method)) {
            return false;
        }

        return !$method($arguments);
    }
}
