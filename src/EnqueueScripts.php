<?php
namespace KobaltDigital;

class EnqueueScripts
{
    public function __construct()
    {
        if (!defined('ABSPATH')) {
            exit;
        }

        add_action('wp_enqueue_scripts', [$this, 'wpEnqueueScripts']);
        add_action('admin_enqueue_scripts', [$this, 'adminEnqueueScripts']);
    }

    public function wpEnqueueScripts()
    {
        collect(config('styles.theme.enqueue'))->each([$this, 'enqueueStyle']);

        collect(config('scripts.theme.enqueue.header'))->each(fn($arguments, $key) => $this->enqueueScript($arguments, $key, false));
        collect(config('scripts.theme.enqueue.footer'))->each(fn($arguments, $key) => $this->enqueueScript($arguments, $key, true));
    }

    public function adminEnqueueScripts()
    {
        collect(config('styles.admin.enqueue'))->each([$this, 'enqueueStyle']);

        collect(config('scripts.admin.enqueue.header'))->each(fn($arguments, $key) => $this->enqueueScript($arguments, $key, false));
        collect(config('scripts.admin.enqueue.footer'))->each(fn($arguments, $key) => $this->enqueueScript($arguments, $key, true));
    }

    public function enqueueStyle($arguments, $key)
    {
        if (is_array($arguments)) {
            wp_enqueue_style($key, ...$arguments);
            return;
        }

        wp_enqueue_style($key, $arguments, [], cacheKey(), 'all');
    }

    public function enqueueScript($arguments, $key, $footer = false)
    {
        if (is_array($arguments)) {
            wp_enqueue_script($key, ...$arguments);
            return;
        }

        wp_enqueue_script($key, $arguments, [], cacheKey(), $footer);
    }
}
