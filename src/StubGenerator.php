<?php
namespace KobaltDigital;

use Illuminate\Support\Str;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;

class StubGenerator extends Command
{
    /**
     * The filesystem instance.
     *
     * @var \Illuminate\Filesystem\Filesystem
     */
    protected Filesystem $files;

    /**
     * Reserved names that cannot be used for generation.
     *
     * @var string[]
     */
    protected array $reservedNames = [
        '__halt_compiler',
        'abstract',
        'and',
        'array',
        'as',
        'break',
        'callable',
        'case',
        'catch',
        'class',
        'clone',
        'const',
        'continue',
        'declare',
        'default',
        'die',
        'do',
        'echo',
        'else',
        'elseif',
        'empty',
        'enddeclare',
        'endfor',
        'endforeach',
        'endif',
        'endswitch',
        'endwhile',
        'eval',
        'exit',
        'extends',
        'final',
        'finally',
        'fn',
        'for',
        'foreach',
        'function',
        'global',
        'goto',
        'if',
        'implements',
        'include',
        'include_once',
        'instanceof',
        'insteadof',
        'interface',
        'isset',
        'list',
        'namespace',
        'new',
        'or',
        'print',
        'private',
        'protected',
        'public',
        'require',
        'require_once',
        'return',
        'static',
        'switch',
        'throw',
        'trait',
        'try',
        'unset',
        'use',
        'var',
        'while',
        'xor',
        'yield',
    ];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->files = new Filesystem;
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        // First we need to ensure that the given name is not a reserved word within the PHP
        // language and that the class name will actually be valid. If it is not valid we
        // can error now and prevent from polluting the filesystem using invalid files.
        if ($this->isReservedName($this->getNameInput())) {
            $this->error('The name "' . $this->getNameInput() . '" is reserved by PHP.');

            return;
        }

        collect($this->getStubs())->each(function ($options, $path) {

            extract($options, EXTR_OVERWRITE);

            $name = $this->qualifyClass($this->getNameInput(), $extension);

            $path = $this->getPath($name, $path, $extension);

            // Next, We will check to see if the class already exists. If it does, we don't want
            // to create the class and overwrite the user's code. So, we will bail out so the
            // code is untouched. Otherwise, we will continue generating this class' files.
            if ((! $this->hasOption('force') ||
                  ! $this->option('force')) &&
                  $this->alreadyExists($path)) {
                  $this->error($type.' already exists!');

                return false;
            }

            // Next, we will generate the path to the location where this class' file should get
            // written. Then, we will build the class and make the proper replacements on the
            // stub files so that it gets the correctly formatted namespace and class name.
            $this->makeDirectory($path);

            $stub = $this->files->get($stub);

            $this->files->put($path, $this->sortImports($this->replaceVariables($stub, $name)));

            $this->info($type . ' created successfully.');
        });
        if (method_exists($this, 'afterCreate')) {
            $this->afterCreate();
        }
    }

    /**
     * Alphabetically sorts the imports for the given stub.
     *
     * @param string $stub
     * @return string
     */
    protected function sortImports(string $stub): string
    {
        if (preg_match('/(?P<imports>(?:use [^;]+;$\n?)+)/m', $stub, $match)) {
            $imports = explode("\n", trim($match['imports']));

            sort($imports);

            return str_replace(trim($match['imports']), implode("\n", $imports), $stub);
        }

        return $stub;
    }

    /**
     * Replace the namespace for the given stub.
     *
     * @param string $stub
     * @param string $name
     * @return $this
     */
    protected function replaceVariables(string $stub, string $name)
    {
        if (method_exists($this, 'getVariables')) {
            $variables = $this->getVariables();
        }
        $root = Str::replaceLast('\\', '', $this->rootNamespace());

        $namespace = Str::remove($root, $this->getNamespace($name));

        return collect(array_merge($variables ?? [], [
          'root_namespace' => $root,
          'namespace' => $namespace,
          'class' => str_replace($this->getNamespace($name).'\\', '', $name)
        ]))->reduce(function ($acc, $value, $key) {
            return str_replace("{{ $key }}", $value, $acc);
        }, $stub);
    }

    /**
     * Get the full namespace for a given class, without the class name.
     *
     * @param string $name
     * @return string
     */
    protected function getNamespace(string $name): string
    {
        return trim(implode('\\', array_slice(explode('\\', $name), 0, -1)), '\\');
    }

    /**
     * Build the directory for the class if necessary.
     *
     * @param string $path
     * @return string
     */
    protected function makeDirectory(string $path): string
    {
        if (! $this->files->isDirectory(dirname($path))) {
            $this->files->makeDirectory(dirname($path), 0777, true, true);
        }

        return $path;
    }


    /**
     * Parse the class name and format according to the root namespace.
     *
     * @param string $name
     * @param string $extension
     * @return string
     */
    protected function qualifyClass(string $name, string $extension = '.php'): string
    {
        $name = ltrim($name, '\\/');

        $name = str_replace('/', '\\', $name);

        $name = $extension === ".php"
          ? Str::studly($name)
          : Str::lower($name);

        $rootNamespace = $this->rootNamespace();

        if (Str::startsWith($name, $rootNamespace)) {
            return $name;
        }

        return $this->qualifyClass(
            $this->getDefaultNamespace(trim($rootNamespace, '\\')).'\\'.$name
        );
    }

    /**
     * Get the destination class path.
     *
     * @param string $name
     * @param null $folder
     * @param string $extension
     * @return string
     */
    protected function getPath(string $name, $folder = null, string $extension = '.php'): string
    {
        $path = str_replace('/vendor/kobalt/boilerplate', '', $this->laravel['path']);
        $name = Str::replaceFirst($this->rootNamespace(), '', $name);

        return $path . '/src/' . $folder . '/' . str_replace('\\', '/', $name) . $extension;
    }

    /**
     * Determine if the class already exists.
     *
     * @param $file
     * @return bool
     */
    protected function alreadyExists($file): bool
    {
        return $this->files->exists($file);
    }

    /**
     * Get the root namespace for the class.
     *
     * @return string
     */
    protected function rootNamespace(): string
    {
        return $this->laravel->getNamespace();
    }

    /**
     * Get the default namespace for the class.
     *
     * @param string $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace(string $rootNamespace): string
    {
        return $rootNamespace;
    }


    /**
     * Get the desired class name from the input.
     *
     * @return string
     */
    protected function getNameInput(): string
    {
        return trim($this->argument('name'));
    }

    /**
     * Checks whether the given name is reserved.
     *
     * @param string $name
     * @return bool
     */
    protected function isReservedName(string $name): bool
    {
        $name = strtolower($name);

        return in_array($name, $this->reservedNames);
    }

    protected function parse($stub, $data)
    {
        return collect($data)->reduce(function ($acc, $value, $key) {
            return str_replace("{{ $key }}", $value, $acc);
        }, $stub);
    }

    protected function output($file)
    {
        file_put_contents($this->laravel->basePath(sprintf('Commands/%s.php', Str::studly($this->argument('name')))), $file);
    }
}
