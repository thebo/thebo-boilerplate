<?php

namespace {{ root_namespace }}\Acf\Blocks{{ namespace }};

use Illuminate\View\View;
use KobaltDigital\Extend\Block;

class {{ class }} extends Block
{
    /**
     * A unique name that identifies the block (without namespace). For example ‘testimonial’.
     *
     * Note: A block name can only contain lowercase alphanumeric characters and dashes,
     * and must begin with a letter.
     *
     * @var string
     */
    protected string $name = '{{ name }}';

    /**
     * The display title for your block. For example ‘Testimonial’.
     *
     * @return string
     */
    public function getTitle(): string
    {
        return __('{{ title }}', themeTextDomain());
    }

    /**
     * This is a short description for your block.
     *
     * @return string
     */
    public function getDescription(): string
    {
        return __('', themeTextDomain());
    }

    /**
     * Blocks are grouped into categories to help users browse and discover them.
     * The core provided categories are [ common | formatting | layout | widgets | embed ].
     *
     * @var string
     */
    protected string $category = '';

    /**
     * An icon property can be specified to make it easier to identify a block.
     * These can be any of WordPress’ Dashicons, or a custom svg element.
     *
     * @var string|array
     */
    protected $icon = '';

    /**
     * The default block alignment. Available settings are
     * “left”, “center”, “right”, “wide” and “full”.
     * Defaults to an empty string.
     *
     * @var string
     */
    protected string $align = '';

    /**
     * The display mode for your block. Available settings are “auto”, “preview” and “edit”.
     * Defaults to “preview”.
     *
     * auto: Preview is shown by default but changes to edit form when block is selected.
     * preview: Preview is always shown. Edit form appears in sidebar when block is selected.
     * edit: Edit form is always shown.
     *
     * @var string
     */
    protected string $mode = 'preview';

    /**
     * An array of search terms to help user discover the block while searching.
     *
     * @var array
     */
    protected array $keywords = [];

    /**
     * An array of post types to restrict this block type to.
     *
     * @var array
     */
    protected array $post_types = [];

    /**
     * An array of features to support.
     *
     * @var array
     */
    protected array $supports = [];

    /**
     * The view that will be rendered.
     *
     * @return View
     */
    protected function render(): View
    {
        return view('blocks.{{ view }}');
    }
}
