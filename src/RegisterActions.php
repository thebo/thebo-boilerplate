<?php
namespace KobaltDigital;

class RegisterActions
{
    public function __construct()
    {
        $actions = array_merge(
            remember('user/actions', function () {
                return getClassesInDirectory(get_theme_file_path('App/Actions/'), config('theme.namespace') . 'Actions');
            }),
            remember('default/actions', function () {
                return getClassesInDirectory(__DIR__ . '/Actions', 'KobaltDigital\Actions');
            })
        );

        collect($actions)->each(function ($class) {
            if (!$class) {
                return;
            }

            if (!class_exists($class)) {
                return;
            }

            $action = new $class();

            if (!$action->getAction()) {
                return;
            }

            $actions = is_array($action->getAction()) ? $action->getAction() : [$action->getAction()];

            $reflector = new \ReflectionClass($class);
            $accepted_args = count($reflector->getMethod('handle')->getParameters());

            collect($actions)->each(function ($name) use ($action, $accepted_args) {
                add_action($name, [$action, 'handle'], $action->getPriority(), $accepted_args);
            });
        });
    }
}
