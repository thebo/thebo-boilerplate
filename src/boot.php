<?php

use KobaltDigital\DequeueScripts;
use KobaltDigital\EnqueueScripts;
use KobaltDigital\RegisterFilters;
use KobaltDigital\RegisterActions;

new DequeueScripts();
new EnqueueScripts();

new RegisterActions();
new RegisterFilters();
