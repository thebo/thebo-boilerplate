<?php

namespace KobaltDigital\Actions\Register;

use KobaltDigital\Extend\Action;

class SideBars extends Action
{
    /**
     * The name of the action to add the callback to
     *
     * @var string
     */
    protected string $action = 'widgets_init';

    /**
     * Used to specify the order in which the functions associated with a particular action
     * are executed. Lower numbers correspond with earlier execution, and functions with the
     * same priority are executed in the order in which they were added to the action.
     *
     * @var int
     */
    protected int $priority = 10;

    /**
     * The callback to be run when the action is called.
     *
     * @return void
     */
    public function handle(): void
    {
        $sidebars = remember('user/sidebars', function () {
            return getClassesInDirectory(get_theme_file_path('App/Sidebars/'), config('theme.namespace') . 'Sidebars');
        });

        collect($sidebars)->each(function ($class) {
            if (!$class) {
                return;
            }

            if (!class_exists($class)) {
                return;
            }

            $sidebar = new $class();

            if (!$sidebar->getId()) {
                return;
            }

            register_sidebar([
                'id' => $sidebar->getId(),
                'name' => $sidebar->getSidebar(),
                'description' => $sidebar->getDescription(),
                'class' => $sidebar->getClass(),
                'before_widget' => $sidebar->getBeforeWidget(),
                'after_widget' => $sidebar->getAfterWidget(),
                'before_title' => $sidebar->getBeforeTitle(),
                'after_title' => $sidebar->getAfterTitle(),
                'show_in_rest' => $sidebar->getShowInRest(),
            ]);
        });
    }
}
