<?php

namespace KobaltDigital\Actions\Register;

use KobaltDigital\Extend\Action;

class ShortCodes extends Action
{
    /**
     * The name of the action to add the callback to
     *
     * @var string
     */
    protected string $action = 'init';

    /**
     * Used to specify the order in which the functions associated with a particular action
     * are executed. Lower numbers correspond with earlier execution, and functions with the
     * same priority are executed in the order in which they were added to the action.
     *
     * @var int
     */
    protected int $priority = 10;

    /**
     * The callback to be run when the action is called.
     *
     * @return void
     * @throws \ReflectionException
     */
    public function handle(): void
    {
        $shortCodes = remember('user/short_codes', function () {
            return getClassesInDirectory(get_theme_file_path('App/Shortcodes/'), config('theme.namespace') . 'Shortcodes');
        });

        collect($shortCodes)->each(function ($class) {
            if (!$class) {
                return;
            }

            if (!class_exists($class)) {
                return;
            }

            $shortcode = new $class();

            if (!$shortcode->getShortcode()) {
                return;
            }

            add_shortcode($shortcode->getShortcode(), [$shortcode, 'handle']);
        });
    }
}
