<?php

namespace KobaltDigital\Actions\Register;

use KobaltDigital\Extend\Action;

class Taxonomies extends Action
{
    /**
     * The name of the action to add the callback to
     *
     * @var string
     */
    protected string $action = 'init';

    /**
     * Used to specify the order in which the functions associated with a particular action
     * are executed. Lower numbers correspond with earlier execution, and functions with the
     * same priority are executed in the order in which they were added to the action.
     *
     * @var int
     */
    protected int $priority = 1;

    /**
     * The callback to be run when the action is called.
     *
     * @return void
     */
    public function handle(): void
    {
        $taxonomies = remember('user/taxonomies', function () {
            return getClassesInDirectory(get_theme_file_path('App/Taxonomies/'), config('theme.namespace') . 'Taxonomies');
        });

        collect($taxonomies)->each(function ($class) {
            if (!$class) {
                return;
            }

            if (!class_exists($class)) {
                return;
            }

            $taxonomy = new $class();

            if (!$taxonomy->getTaxonomy()) {
                return;
            }

            register_taxonomy($taxonomy->getTaxonomy(), $taxonomy->getPostType(), [
                'labels' => $taxonomy->getLabels(),
                'description' => $taxonomy->getDescription(),
                'public' => $taxonomy->getPublic(),
                'publicly_queryable' => $taxonomy->getPubliclyQueryable(),
                'show_ui' => $taxonomy->getShowUi(),
                'show_in_menu' => $taxonomy->getShowInMenu(),
                'show_in_nav_menus' => $taxonomy->getShowInNavMenus(),
                'hierarchical' => $taxonomy->getHierarchical(),
                'rewrite' => $taxonomy->getRewrite(),
                'capabilities' => $taxonomy->getCapabilities(),
                'show_in_rest' => $taxonomy->getShowInRest(),
                'rest_base' => $taxonomy->getRestBase(),
            ]);
        });
    }
}
