<?php

namespace KobaltDigital\Actions\Register;

use KobaltDigital\View\Blade;
use KobaltDigital\Extend\Action;

class BladeDirectives extends Action
{
    /**
     * The name (or array) of the action(s) to add the callback to
     *
     * @var string
     */
    protected string $action = 'init';

    /**
     * Used to specify the order in which the functions associated with a particular action
     * are executed. Lower numbers correspond with earlier execution, and functions with the
     * same priority are executed in the order in which they were added to the action.
     *
     * @var int
     */
    protected int $priority = 1;

    /**
     * The callback to be run when the action is called.
     *
     * @return void
     * @throws \ReflectionException
     */
    public function handle(): void
    {
        $directives = array_merge(
            remember('user/directives', function () {
                return getClassesInDirectory(get_theme_file_path('App/Directives/'), config('theme.namespace') . 'Directives');
            }),
            remember('default/directives', function () {
                return getClassesInDirectory(dirname(dirname(__DIR__)) . '/Directives', 'KobaltDigital\Directives');
            })
        );

        collect($directives)->each(function ($class) {
            if (!$class) {
                return;
            }

            if (!class_exists($class)) {
                return;
            }

            $directive = new $class();

            Blade::directive($directive->getDirective(), [$directive, 'handle']);
        });
    }
}
