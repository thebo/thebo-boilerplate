<?php

namespace KobaltDigital\Actions\Register;

use KobaltDigital\Extend\Action;

class PostTypes extends Action
{
    /**
     * The name of the action to add the callback to
     *
     * @var string
     */
    protected string $action = 'init';

    /**
     * Used to specify the order in which the functions associated with a particular action
     * are executed. Lower numbers correspond with earlier execution, and functions with the
     * same priority are executed in the order in which they were added to the action.
     *
     * @var int
     */
    protected int $priority = 1;

    /**
     * The callback to be run when the action is called.
     *
     * @return void
     */
    public function handle(): void
    {
        $postTypes = remember('user/post_types', function () {
            return getClassesInDirectory(get_theme_file_path('App/PostTypes/'), config('theme.namespace') . 'PostTypes');
        });

        collect($postTypes)->each(function ($class) {
            if (!$class) {
                return;
            }

            if (!class_exists($class)) {
                return;
            }

            $postType = new $class();

            register_post_type($postType->getPostType(), [
                'label' => $postType->getLabel(),
                'labels' => $postType->getLabels(),
                'description' => $postType->getDescription(),
                'public' => $postType->getPublic(),
                'publicly_queryable' => $postType->getPubliclyQueryable(),
                'show_ui' => $postType->getShowUi(),
                'show_in_menu' => $postType->getShowInMenu(),
                'menu_position' => $postType->getMenuPosition(),
                'menu_icon' => $postType->getMenuIcon(),
                'capability_type' => $postType->getCapabilityType(),
                'capabilities' => $postType->getCapabilities(),
                'hierarchical' => $postType->getHierarchical(),
                'supports' => $postType->getSupports(),
                'taxonomies' => $postType->getTaxonomies(),
                'has_archive' => $postType->getHasArchive(),
                'rewrite' => $postType->getRewrite(),
                'can_export' => $postType->getCanExport(),
                'show_in_rest' => $postType->getShowInRest(),
                'rest_base' => $postType->getRestBase(),
                'delete_with_user' => $postType->getDeleteWithUser(),
            ]);
        });
    }
}
