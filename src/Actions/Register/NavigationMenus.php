<?php

namespace KobaltDigital\Actions\Register;

use KobaltDigital\Extend\Action;

class NavigationMenus extends Action
{
    /**
     * The name of the action to add the callback to
     *
     * @var string
     */
    protected string $action = 'after_setup_theme';

    /**
     * Used to specify the order in which the functions associated with a particular action
     * are executed. Lower numbers correspond with earlier execution, and functions with the
     * same priority are executed in the order in which they were added to the action.
     *
     * @var int
     */
    protected int $priority = 10;

    /**
     * The callback to be run when the action is called.
     *
     * @return void
     */
    public function handle(): void
    {
        register_nav_menus(config('menus'));
    }
}
