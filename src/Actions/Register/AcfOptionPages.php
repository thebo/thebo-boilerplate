<?php

namespace KobaltDigital\Actions\Register;

use KobaltDigital\Extend\Action;

class AcfOptionPages extends Action
{
    /**
     * The name of the action to add the callback to
     *
     * @var string
     */
    protected string $action = 'init';

    /**
     * Used to specify the order in which the functions associated with a particular action
     * are executed. Lower numbers correspond with earlier execution, and functions with the
     * same priority are executed in the order in which they were added to the action.
     *
     * @var int
     */
    protected int $priority = 10;

    /**
     * The callback to be run when the action is called.
     *
     * @return void
     */
    public function handle(): void
    {
        if (!function_exists('acf_add_options_page')) {
            return;
        }

        $pages = remember('user/options-pages', function () {
            return getClassesInDirectory(get_theme_file_path('App/Acf/OptionsPages/'), config('theme.namespace') . 'Acf\OptionsPages');
        });

        collect($pages)->each(function ($class) {
            if (!$class) {
                return;
            }

            if (!class_exists($class)) {
                return;
            }

            $page = new $class();

            if (!$page->getSlug()) {
                return;
            }

            $data = [
                'page_title' => $page->getPageTitle(),
                'menu_title' => $page->getMenuTitle(),
                'menu_slug' => $page->getSlug(),
                'redirect' => $page->getRedirect(),
            ];

            if ($page->getCapability()) {
                $data['capability'] = $page->getCapability();
            }

            if ($page->getParent()) {
                $data['parent_slug'] = $page->getParent();
                acf_add_options_sub_page($data);
                return;
            }

            acf_add_options_page($data);
        });
    }
}
