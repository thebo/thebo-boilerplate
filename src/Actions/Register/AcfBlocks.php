<?php

namespace KobaltDigital\Actions\Register;

use KobaltDigital\EnqueueScripts;
use KobaltDigital\Extend\Action;

class AcfBlocks extends Action
{
    /**
     * The name of the action to add the callback to
     *
     * @var string
     */
    protected string $action = 'acf/init';

    /**
     * Used to specify the order in which the functions associated with a particular action
     * are executed. Lower numbers correspond with earlier execution, and functions with the
     * same priority are executed in the order in which they were added to the action.
     *
     * @var int
     */
    protected int $priority = 10;

    /**
     * The callback to be run when the action is called.
     *
     * @return void
     * @throws \ReflectionException
     */
    public function handle(): void
    {
        if (!function_exists('acf_register_block_type')) {
            return;
        }

        $blocks = remember('user/blocks', function () {
            return getClassesInDirectory(get_theme_file_path('App/Acf/Blocks/'), config('theme.namespace') . 'Acf\Blocks');
        });

        collect($blocks)->each(function ($class) {
            if (!$class) {
                return;
            }

            if (!class_exists($class)) {
                return;
            }

            $block = new $class();

            if (!$block->getName()) {
                return;
            }

            acf_register_block_type([
                'name' => $block->getName(),
                'title' => $block->getTitle(),
                'description' => $block->getDescription(),
                'category' => $block->getCategory(),
                'icon' => $block->getIcon(),
                'keywords' => $block->getKeywords(),
                'mode' => $block->getMode(),
                'align' => $block->getAlign(),
                'enqueue_assets' => function () {
                    (new EnqueueScripts())->wpEnqueueScripts();
                },
                'supports' => $block->getSupports(),
                'render_callback' => [$block, 'handle'],
            ]);
        });
    }
}
