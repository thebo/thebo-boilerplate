<?php

namespace KobaltDigital\Actions\Admin;

use KobaltDigital\Extend\Action;

class AddClearViewCacheButton extends Action
{
    /**
     * The name of the action to add the callback to
     *
     * @var string
     */
    protected string $action = 'admin_bar_menu';

    /**
     * Used to specify the order in which the functions associated with a particular action
     * are executed. Lower numbers correspond with earlier execution, and functions with the
     * same priority are executed in the order in which they were added to the action.
     *
     * @var int
     */
    protected int $priority = 300;

    /**
     * The callback to be run when the action is called.
     *
     * @param $navigation
     * @return void
     */
    public function handle($navigation): void
    {
        $navigation->add_menu([
            'id'    => 'my-item',
            'title' => __('Clear View Cache'),
            'href'  => sprintf('%s?%s=true', get_site_url(), 'clear_blade_cache'),
            'meta'  => [
                'title' => __('Clear View Cache'),
            ],
        ]);
    }
}
