<?php

namespace KobaltDigital\Actions\Admin;

use KobaltDigital\Extend\Action;

class RedirectAfterClearViewCache extends Action
{
    /**
     * The name of the action to add the callback to
     *
     * @var string
     */
    protected string $action = 'init';

    /**
     * Used to specify the order in which the functions associated with a particular action
     * are executed. Lower numbers correspond with earlier execution, and functions with the
     * same priority are executed in the order in which they were added to the action.
     *
     * @var int
     */
    protected int $priority = 10;

    /**
     * The callback to be run when the action is called.
     *
     * @return void
     */
    public function handle(): void
    {
        if (isset($_GET['clear_blade_cache']) &&
            $_GET['clear_blade_cache'] === "true" &&
            is_user_logged_in()
        ) {
            global $wp;
            array_map('unlink', array_filter((array) glob(sprintf("%s/*", config('blade.cache')))));
            wp_redirect(home_url($wp->request));
            die();
        }
    }
}
