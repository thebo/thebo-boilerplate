<?php

namespace KobaltDigital\Directives\Acf;

use KobaltDigital\Extend\Directive;

class Repeater extends Directive
{
    /**
     * Directive name to be replaced in blade files.
     *
     * @var string
     */
    protected string $directive = 'repeater';

    /**
     * The callback function to call when the directive is run.
     *
     * @param $expression
     * @return string
     */
    public function handle($expression): string
    {
        return "<?php if (have_rows($expression)): while(have_rows($expression)): the_row(); ?>";
    }
}
