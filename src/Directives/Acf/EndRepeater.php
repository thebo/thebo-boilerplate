<?php

namespace KobaltDigital\Directives\Acf;

use KobaltDigital\Extend\Directive;

class EndRepeater extends Directive
{
    /**
     * Directive name to be replaced in blade files.
     *
     * @var string
     */
    protected string $directive = 'endrepeater';

    /**
     * The callback function to call when the directive is run.
     *
     * @param $expression
     * @return string
     */
    public function handle($expression): string
    {
        return "<?php endwhile; endif; ?>";
    }
}
