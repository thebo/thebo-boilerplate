<?php

namespace KobaltDigital\Directives\Acf;

use KobaltDigital\Extend\Directive;

class SubField extends Directive
{
    /**
     * Directive name to be replaced in blade files.
     *
     * @var string
     */
    protected string $directive = 'subfield';

    /**
     * The callback function to call when the directive is run.
     *
     * @param $expression
     * @return string
     */
    public function handle($expression): string
    {
        return "<?php the_sub_field($expression) ?>";
    }
}
