<?php

namespace KobaltDigital\Directives;

use KobaltDigital\Extend\Directive;

class GlobalVariable extends Directive
{
    /**
     * Directive name to be replaced in blade files.
     *
     * @var string
     */
    protected string $directive = 'global';

    /**
     * The callback function to call when the directive is run.
     *
     * @param $expression
     * @return string
     */
    public function handle($expression): string
    {
        $expression = str_replace('\'', '', $expression);
        return "<?php global $$expression; ?>";
    }
}
