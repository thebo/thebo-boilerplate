<?php

namespace KobaltDigital\Directives\Utils;

use KobaltDigital\Extend\Directive;

class Ray extends Directive
{
    /**
     * Directive name to be replaced in blade files.
     *
     * @var string
     */
    protected string $directive = 'ray';

    /**
     * The callback function to call when the directive is run.
     *
     * @param $expression
     * @return string
     */
    public function handle($expression): string
    {
        return "<?php ray($expression); ?>";
    }
}
