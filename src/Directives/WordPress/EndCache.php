<?php

namespace KobaltDigital\Directives\WordPress;

use KobaltDigital\Extend\Directive;

class EndCache extends Directive
{
    /**
     * Directive name to be replaced in blade files.
     *
     * @var string
     */
    protected string $directive = 'endcache';

    /**
     * The callback function to call when the directive is run.
     *
     * @return string
     */
    public function handle(): string
    {
        return "<?php
                set_transient(\$__CACHE_KEY, ob_get_clean(), 60 * 60 * 24 * 30);
                echo get_transient(\$__CACHE_KEY);
                unset(\$__CACHE_KEY);
            }
        ?>";
    }
}
