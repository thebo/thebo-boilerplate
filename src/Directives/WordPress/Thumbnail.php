<?php

namespace KobaltDigital\Directives\WordPress;

use KobaltDigital\Extend\Directive;

class Thumbnail extends Directive
{
    /**
     * Directive name to be replaced in blade files.
     *
     * @var string
     */
    protected string $directive = 'thumbnail';

    /**
     * The callback function to call when the directive is run.
     *
     * @param null $expression
     * @return string
     */
    public function handle($expression = null): string
    {
        return "<?php the_post_thumbnail($expression) ?>";
    }
}
