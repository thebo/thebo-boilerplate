<?php

namespace KobaltDigital\Directives\WordPress;

use KobaltDigital\Extend\Directive;

class Menu extends Directive
{
    /**
     * Directive name to be replaced in blade files.
     *
     * @var string
     */
    protected string $directive = 'menu';

    /**
     * The callback function to call when the directive is run.
     *
     * @param $expression
     * @return string
     */
    public function handle($expression): string
    {
        return "<?php wp_nav_menu($expression); ?>";
    }
}
