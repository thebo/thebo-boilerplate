<?php

namespace KobaltDigital\Directives\WordPress;

use KobaltDigital\Extend\Directive;

class EndQuery extends Directive
{
    /**
     * Directive name to be replaced in blade files.
     *
     * @var string
     */
    protected string $directive = 'endquery';

    /**
     * The callback function to call when the directive is run.
     *
     * @return string
     */
    public function handle(): string
    {
        return "<?php endwhile; wp_reset_postdata(); endif; ?>";
    }
}
