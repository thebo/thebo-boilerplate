<?php

namespace KobaltDigital\Directives\WordPress;

use KobaltDigital\Extend\Directive;

class Cache extends Directive
{
    /**
     * Directive name to be replaced in blade files.
     *
     * @var string
     */
    protected string $directive = 'cache';

    /**
     * The callback function to call when the directive is run.
     *
     * @param $expression
     * @return string
     */
    public function handle($expression): string
    {
        return "<?php
            if (get_transient($expression)) {
                echo get_transient($expression);
            } else {
                ob_start();
                \$__CACHE_KEY = $expression;
            ?>";
    }
}
