<?php

namespace KobaltDigital\WordPress\Directives;

use KobaltDigital\Extend\Directive;

class DoAction extends Directive
{
    /**
     * Directive name to be replaced in blade files.
     *
     * @var string
     */
    protected string $directive = 'do_action';

    /**
     * The callback function to call when the directive is run.
     *
     * @param $expression
     * @return string
     */
    public function handle($expression): string
    {
        return "<?php do_action($expression); ?>";
    }
}
