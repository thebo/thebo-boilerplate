<?php

namespace KobaltDigital\Directives\WordPress;

use KobaltDigital\Extend\Directive;

class Avatar extends Directive
{
    /**
     * Directive name to be replaced in blade files.
     *
     * @var string
     */
    protected string $directive = 'avatar';

    /**
     * The callback function to call when the directive is run.
     *
     * @param null $expression
     * @return string
     */
    public function handle($expression = null): string
    {
        $expression = $expression ?: 'get_the_author_ID()';
        return "<?php echo get_avatar_url($expression) ?>";
    }
}
