<?php

namespace KobaltDigital\Directives\WordPress;

use KobaltDigital\Extend\Directive;

class EmptyQuery extends Directive
{
    /**
     * Directive name to be replaced in blade files.
     *
     * @var string
     */
    protected string $directive = 'emptyquery';

    /**
     * The callback function to call when the directive is run.
     *
     * @return string
     */
    public function handle(): string
    {
        return "<?php endwhile; else: \$i = 0; while (isset(\$i)): unset(\$i) ?>";
    }
}
