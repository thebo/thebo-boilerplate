<?php

namespace KobaltDigital\Directives\WordPress;

use KobaltDigital\Extend\Directive;

class Query extends Directive
{
    /**
     * Directive name to be replaced in blade files.
     *
     * @var string
     */
    protected string $directive = 'query';

    /**
     * The callback function to call when the directive is run.
     *
     * @param null $expression
     * @return string
     */
    public function handle($expression = null): string
    {
        if ($expression) {
            return sprintf('<?php
              $__QUERY = %s;
              $__QUERY = is_array($__QUERY) ? new \WP_Query(array_merge([
                  "paged" => get_query_var("paged")
              ], $__QUERY)) : $__QUERY;
              if ($__QUERY->have_posts()): while($__QUERY->have_posts()): $__QUERY->the_post();
            ?>', $expression);
        }

        return '<?php if (have_posts()): while(have_posts()): the_post(); ?>';
    }
}
