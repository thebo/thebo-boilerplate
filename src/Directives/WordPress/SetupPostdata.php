<?php

namespace KobaltDigital\WordPress\Directives;

use KobaltDigital\Extend\Directive;

class SetupPostdata extends Directive
{
    /**
     * Directive name to be replaced in blade files.
     *
     * @var string
     */
    protected string $directive = 'setup_postdata';

    /**
     * The callback function to call when the directive is run.
     *
     * @param $expression
     * @return string
     */
    public function handle($expression): string
    {
        return "<?php
            global \$post;
            \$post = $expression;
            setup_postdata(\$post);
        ?>";
    }
}
