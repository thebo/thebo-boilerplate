<?php

namespace KobaltDigital\Directives\WordPress;

use KobaltDigital\Extend\Directive;

class Pagination extends Directive
{
    /**
     * Directive name to be replaced in blade files.
     *
     * @var string
     */
    protected string $directive = 'pagination';

    /**
     * The callback function to call when the directive is run.
     *
     * @param null $expression
     * @return string
     */
    public function handle($expression = null): string
    {
        return "<?php
            if (isset(\$__QUERY)) {
                echo paginate_links(array_merge([
                    'current' => max(1, get_query_var('paged')),
                    'total' => \$__QUERY->max_num_pages
                ], $expression));
            } else {
                echo paginate_links($expression);
            }
        ?>";
    }
}
