<?php

use KobaltDigital\View\Blade;

function view(string $view, array $data = []): Illuminate\View\View
{
    return Blade::getInstance()
        ->viewFactory
        ->make($view, $data);
}
