<?php

use Illuminate\Support\Str;

if (!function_exists('getFilters')) {
    return;
}

function getFilters()
{
    $dist = Str::before(get_theme_file_path(), 'wp-content');

    $files = getFilesInDirectory($dist, '.php');

    $filters = [];

    foreach ($files as $file) {
        if (!file_exists($file)) {
            continue;
        }

        $content = file_get_contents($file);

        $docRegex = '/^(\h*\/\*\*(?:\R\h*\*.*)*\R\h*\*\/)?\n?(.*)(apply_filters\(\s?\'(.*?)\'\s?,\s?(.*?)\s?\))/m';
        preg_match_all($docRegex, $content, $matches, PREG_SET_ORDER, 0);

        if (!$matches) {
            continue;
        }

        foreach ($matches as $match) {
            if (!$match) {
                continue;
            }

            if (isset($filters[$match[4]]) && str_replace(' ', '', $filters[$match[4]]['docblock']) != "") {
                continue;
            }

            $filters[$match[4]] = [
                'docblock' => preg_replace('/^\s*/m', '    ', $match[1]),
                'filter' => $match[4],
                'file' => $file,
                'parameters' => parseDocblockParameters($match[1]),
            ];
        }
    }

    return $filters;
}
