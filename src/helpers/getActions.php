<?php

use Illuminate\Support\Str;

if (!function_exists('getActions')) {
    return;
}

function getActions()
{
    $dist = Str::before(get_theme_file_path(), 'wp-content');

    $files = getFilesInDirectory($dist, '.php');

    $actions = [];

    foreach ($files as $file) {
        if (!file_exists($file)) {
            continue;
        }

        $content = file_get_contents($file);

        $docRegex = '/^(\h*\/\*\*(?:\R\h*\*.*)*\R\h*\*\/)?\n?(.*)(do_action\(\s?\'(.*?)\'\s?,\s?(.*?)\s?\))/m';
        preg_match_all($docRegex, $content, $matches, PREG_SET_ORDER, 0);

        if (!$matches) {
            continue;
        }

        foreach ($matches as $match) {
            if (!$match) {
                continue;
            }

            if (isset($actions[$match[4]]) && str_replace(' ', '', $actions[$match[4]]['docblock']) != "") {
                continue;
            }

            $actions[$match[4]] = [
                'docblock' => preg_replace('/^\s*/m', '    ', $match[1]),
                'action' => $match[4],
                'file' => $file,
                'parameters' => parseDocblockParameters($match[1]),
            ];
        }
    }

    return $actions;
}
