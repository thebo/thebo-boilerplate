<?php

use Illuminate\Support\Str;

if (function_exists('getViewsInDirectory')) {
    return;
}

function getViewsInDirectory($dir)
{
    $views = [];

    if (!is_dir($dir)) {
        return $views;
    }

    foreach (scandir($dir) as $file) {
        if ($file[0] === '.') {
            continue;
        }

        if (is_dir($dir . '/' . $file)) {
            continue;
        }

        if (strpos($file, '.blade.php') === false) {
            continue;
        }

        $views[] = Str::replace('.blade.php', '', $file);
    }
    return $views;
}
