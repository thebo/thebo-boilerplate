<?php

if (function_exists('when')) {
    return;
}

function when($condition, $value)
{
    if (!$condition) {
        return;
    }

    return $value;
}
