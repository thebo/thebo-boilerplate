<?php

if (function_exists('isLocalhost')) {
    return;
}

function isLocalhost(array $whitelist = ['127.0.0.1', '::1']): bool
{
    return isset($_SERVER['REMOTE_ADDR']) && in_array($_SERVER['REMOTE_ADDR'], $whitelist, true);
}
