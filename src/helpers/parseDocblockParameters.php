<?php

if (!function_exists('parseDocblockParameters')) {
    return;
}

function parseDocblockParameters($docblock)
{
    $regex = '/@param\s+(.*?)\s+(.*?)\s+(.*)/m';

    preg_match_all($regex, $docblock, $matches, PREG_SET_ORDER, 0);

    $parameters = [];

    foreach ($matches as $match) {
        $parameters[] = [
            'type' => $match[1],
            'name' => $match[2],
            'description' => $match[3],
        ];
    }

    return $parameters;
}
