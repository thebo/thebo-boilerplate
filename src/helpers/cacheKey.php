<?php

if (function_exists('cacheKey')) {
    return;
}

$__CACHE_KEY;

function cacheKey()
{
    global $__CACHE_KEY;
    return $__CACHE_KEY
        ? $__CACHE_KEY
        : ($__CACHE_KEY = filemtime(get_stylesheet_directory() . '/style.css'));
}
