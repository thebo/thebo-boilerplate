<?php

if (function_exists('themeTextDomain')) {
    return;
}

function themeTextDomain(): string
{
    return wp_get_theme()->get('Name');
}
