<?php

if (function_exists('getClassesInDirectory')) {
    return;
}

function getClassesInDirectory($dir, $namespace)
{
    $classes = [];

    if (!is_dir($dir)) {
        return $classes;
    }

    foreach (scandir($dir) as $file) {
        if ($file[0] === '.') {
            continue;
        }
        if (is_dir($dir . '/' . $file)) {
            $classes = array_merge($classes, getClassesInDirectory($dir . '/' . $file, $namespace . '\\' . $file));
        } else {
            $file = str_replace('.php', '', $file);
            $classes[] = $namespace . '\\' . $file;
        }
    }
    return $classes;
}
