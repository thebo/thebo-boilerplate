<?php

use Illuminate\Support\Str;

if (function_exists('getFilesInDirectory')) {
    return;
}

function getFilesInDirectory($dir, $extension = null)
{
    $files = [];

    if (!is_dir($dir)) {
        return $files;
    }

    foreach (scandir($dir) as $file) {
        if ($file[0] === '.') {
            continue;
        }

        if ($file === 'vendor') {
            continue;
        }

        if (is_dir($dir . $file)) {
            $files = array_merge($files, getFilesInDirectory($dir . $file . '/', $extension));
        } elseif ($extension && Str::endsWith($file, $extension)) {
            $files[] = $dir . $file;
        } elseif (!$extension) {
            $files[] = $dir . $file;
        }
    }
    return $files;
}
