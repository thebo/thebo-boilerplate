<?php

use KobaltDigital\Config;

if (!function_exists('config')) {
    function config(string $key = null, $default = null)
    {
        return Config::get($key);
    }
}
