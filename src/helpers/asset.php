<?php

if (function_exists('asset')) {
    return;
}

function asset(string $asset): string
{
    return sprintf("%s/resources/%s", get_stylesheet_directory_uri(), $asset);
}
