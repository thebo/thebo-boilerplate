<?php

if (function_exists('remember')) {
    return;
}

function remember($key, $callback, $duration = null)
{
    $key = sprintf('%s_%d', $key, cacheKey());

    if (get_transient($key)) {
        return get_transient($key);
    }

    set_transient($key, $callback(), $duration);

    return get_transient($key);
}
