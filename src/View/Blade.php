<?php
namespace KobaltDigital\View;

use Illuminate\Support\Facades\File;
use KobaltDigital\App;

class Blade
{
    public static $instance;

    private $container;
    public $bladeCompiler;
    public $viewFactory;

    private static $templatesDirectory = [];
    private static $compiledDirectory;

    public static function getInstance()
    {
        if (self::$instance) {
            return self::$instance;
        }

        $instance = new self();

        $instance->container = App::getInstance();

        // we have to bind our app class to the interface
        // as the blade compiler needs the `getNamespace()` method to guess Blade component FQCNs
        $instance->container->instance(\Illuminate\Contracts\Foundation\Application::class, $instance->container);


        // Configuration
        // Note that you can set several directories where your templates are located
        $pathsToTemplates = self::$templatesDirectory;
        $pathToCompiledTemplates = self::$compiledDirectory;

        // Dependencies
        $filesystem = new \Illuminate\Filesystem\Filesystem;
        $eventDispatcher = new \Illuminate\Events\Dispatcher($instance->container);

        // Create View Factory capable of rendering PHP and Blade templates
        $viewResolver = new \Illuminate\View\Engines\EngineResolver;
        $instance->bladeCompiler = new \Illuminate\View\Compilers\BladeCompiler($filesystem, $pathToCompiledTemplates);

        $viewResolver->register('blade', function () use ($instance) {
            return new \Illuminate\View\Engines\CompilerEngine($instance->bladeCompiler);
        });

        $viewFinder = new \Illuminate\View\FileViewFinder($filesystem, $pathsToTemplates);
        $instance->viewFactory = new \Illuminate\View\Factory($viewResolver, $viewFinder, $eventDispatcher);

        $instance->viewFactory->setContainer($instance->container);


        \Illuminate\Support\Facades\Facade::setFacadeApplication($instance->container);

        $instance->container->instance(\Illuminate\Contracts\View\Factory::class, $instance->viewFactory);

        $instance->container->alias(
            \Illuminate\Contracts\View\Factory::class,
            (new class extends \Illuminate\Support\Facades\View {
                public static function getFacadeAccessor()
                {
                    return parent::getFacadeAccessor();
                }
            })::getFacadeAccessor()
        );

        $instance->container->instance(\Illuminate\View\Compilers\BladeCompiler::class, $instance->bladeCompiler);

        $instance->container->alias(
            \Illuminate\View\Compilers\BladeCompiler::class,
            (new class extends \Illuminate\Support\Facades\Blade {
                public static function getFacadeAccessor()
                {
                    return parent::getFacadeAccessor();
                }
            })::getFacadeAccessor()
        );

        self::$instance = $instance;

        return self::$instance;
    }

    public static function setCompiledDirectory($path)
    {
        self::$compiledDirectory = $path;
    }

    public static function setTemplateDirectories($path)
    {
        self::$templatesDirectory = $path;
    }

    public static function addTemplatesDirectory($path)
    {
        self::$templatesDirectory[] = $path;
    }

    public static function setClassComponentsDirectory($path)
    {
        $files = File::allFiles($path);
    }

    public static function directive($name, callable $handle)
    {
        self::getInstance()->bladeCompiler->directive($name, $handle);
    }

    public static function if($name, callable $handle)
    {
        self::getInstance()->bladeCompiler->if($name, $handle);
    }

    public static function components($components)
    {
        self::getInstance()->bladeCompiler->components($components);
    }

    public static function view($view, $data = [])
    {
        return self::getInstance()
          ->viewFactory
          ->make($view, $data)
          ->render();
    }

    public static function render($view, $data = [])
    {
        echo self::view($view, $data);
    }
}
