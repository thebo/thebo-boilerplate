<?php

use KobaltDigital\View\Blade;

Blade::setTemplateDirectories(array_merge([
  '../Templates/'
], config('blade.templates')));
