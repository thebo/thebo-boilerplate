<?php

use Illuminate\Support\Str;
use KobaltDigital\View\Blade;

$components = array_merge(
    remember('user/components', function () {
        return collect(getClassesInDirectory(get_theme_file_path('App/Components/'), config('theme.namespace') . 'Components'))->mapWithKeys(function ($class) {
            $name = Str::after($class, 'Components\\');
            $name = Str::replace('\\', '.', $name);
            $name = Str::lower($name);
            return [$name => $class];
        })->toArray();
    })
);

Blade::components($components);
