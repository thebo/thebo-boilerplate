<?php
namespace KobaltDigital;

use Illuminate\Config\Repository;

class Config
{
    public $repository;

    protected static $instance = null;

    public function __construct()
    {
        $this->repository = new Repository();

        $files = glob(get_theme_file_path('config/') . '*.php');

        foreach ($files as $path) {
            if (in_array($path, ['.', '..'])) {
                continue;
            }

            $parts = pathinfo($path);
            $this->repository->set($parts['filename'], require $path);
        }
    }

    protected static function getInstance()
    {
        if (self::$instance) {
            return self::$instance;
        }

        self::$instance = new self();

        return self::$instance;
    }

    public static function get($key)
    {
        return self::getInstance()->repository->get($key);
    }
}
