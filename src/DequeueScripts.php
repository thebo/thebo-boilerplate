<?php
namespace KobaltDigital;

class DequeueScripts
{
    public function __construct()
    {
        if (!defined('ABSPATH')) {
            exit;
        }

        add_action('wp_print_styles', [$this, 'dequeueStyles']);
        add_action('wp_print_scripts', [$this, 'dequeueScripts']);
        add_action('admin_print_styles', [$this, 'adminDequeueStyles']);
        add_action('admin_print_scripts', [$this, 'adminDequeueScripts']);
    }

    public function dequeueStyles()
    {
        collect(config('styles.theme.dequeue'))->each(function ($handle) {
            wp_dequeue_style($handle);
            wp_deregister_style($handle);
        });
    }

    public function dequeueScripts()
    {
        collect(config('scripts.theme.dequeue'))->each(function ($handle) {
            wp_dequeue_script($handle);
            wp_deregister_script($handle);
        });
    }

    public function adminDequeueStyles()
    {
        collect(config('styles.admin.dequeue'))->each(function ($handle) {
            wp_dequeue_style($handle);
            wp_deregister_style($handle);
        });
    }

    public function adminDequeueScripts()
    {
        collect(config('scripts.admin.dequeue'))->each(function ($handle) {
            wp_dequeue_script($handle);
            wp_deregister_script($handle);
        });
    }
}
