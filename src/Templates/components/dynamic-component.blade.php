@props([
  'component' => null
])

@component($component, $attributes->merge([ 'attributes' => new Illuminate\View\ComponentAttributeBag ])->getAttributes())
  {{ $slot }}
@endcomponent
