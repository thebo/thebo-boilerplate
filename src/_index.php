<?php

use KobaltDigital\DequeueScripts;
use KobaltDigital\EnqueueScripts;
use KobaltDigital\RegisterFilters;
use KobaltDigital\RegisterActions;

new DequeueScripts();
new EnqueueScripts();

new RegisterActions();
new RegisterFilters();

load_theme_textdomain('KobaltDigital', __DIR__ . '/languages');

require 'view/_index.php';
