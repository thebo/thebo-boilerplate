<?php

namespace KobaltDigital\Extend;

class Directive
{
    /**
     * Directive name to be replaced in blade files.
     *
     * @return string
     */
    public function getDirective(): string
    {
        return $this->directive ?? '';
    }
}
