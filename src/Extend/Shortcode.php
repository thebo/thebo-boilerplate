<?php

namespace KobaltDigital\Extend;

class Shortcode
{
    /**
     * Shortcode tag to be searched in post content.
     *
     * @return string
     */
    public function getShortcode(): string
    {
        return $this->shortcode ?? '';
    }
}
