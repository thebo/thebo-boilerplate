<?php

namespace KobaltDigital\Extend;

class PostType
{
    /**
     * Post type must not exceed 20 characters and
     * may only contain lowercase alphanumeric
     * characters, dashes, and underscores.
     *
     * @return string
     */
    public function getPostType(): string
    {
        return $this->postType ?? '';
    }

    /**
     * Name of the post type shown in the menu.
     * Usually plural.
     *
     * @return string
     */
    public function getLabel(): string
    {
        return $this->label ?? '';
    }

    /**
     * A short descriptive summary of what the post type is.
     *
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description ?? '';
    }

    /**
     * An array of labels for this post type. If not set,
     * post labels are inherited for non-hierarchical
     * types and page labels for hierarchical ones.
     *
     * @return array
     */
    public function getLabels(): ?array
    {
        return $this->labels ?? null;
    }

    /**
     * Core feature(s) the post type supports.
     * Core features include 'title', 'editor',
     * 'comments', 'revisions', 'trackbacks',
     * 'author', 'excerpt', 'page-attributes',
     * 'thumbnail', 'custom-fields', and 'post-formats'.
     *
     * Additionally, the 'revisions' feature dictates
     * whether the post type will store revisions,
     * and the 'comments' feature dictates whether
     * the comments count will show on the edit screen
     *
     * @return array
     */
    public function getSupports(): array
    {
        return $this->supports ?? [];
    }

    /**
     * An array of taxonomy identifiers that will be registered for the post type.
     *
     * @return array
     */
    public function getTaxonomies(): array
    {
        return $this->taxonomies ?? [];
    }

    /**
     * Whether the post type is hierarchical (e.g. page).
     *
     * @return bool
     */
    public function getHierarchical(): bool
    {
        return $this->hierarchical ?? false;
    }

    /**
     * Whether a post type is intended for use publicly either
     * via the admin interface or by front-end users.
     *
     * @return bool
     */
    public function getPublic(): bool
    {
        return $this->public ?? true;
    }

    /**
     * Whether to generate and allow a UI for managing
     * this post type in the admin.
     *
     * @return bool
     */
    public function getShowUi(): bool
    {
        return $this->showUi ?? true;
    }

    /**
     * Where to show the post type in the admin menu.
     * To work, $showUi must be true.
     *
     * @return bool
     */
    public function getShowInMenu(): bool
    {
        return $this->showInMenu ?? true;
    }

    /**
     * Makes this post type available for
     * selection in navigation menus.
     *
     * @return bool
     */
    public function getShowInNavMenus(): bool
    {
        return $this->showInNavMenus ?? true;
    }

    /**
     * Makes this post type available via the admin bar.
     *
     * @return bool
     */
    public function getShowInAdminBar(): bool
    {
        return $this->showInAdminBar ?? true;
    }

    /**
     * The position in the menu order the post type should appear.
     *
     * @return int
     */
    public function getMenuPosition(): int
    {
        return $this->menuPosition ?? 5;
    }

    /**
     * The URL to the icon to be used for this menu.
     *
     * Pass the name of a Dashicons helper class to use a font icon
     *
     * @return string
     */
    public function getMenuIcon(): string
    {
        return $this->menuIcon ?? '';
    }

    /**
     * Whether to allow this post type to be exported.
     *
     * @return bool
     */
    public function getCanExport(): bool
    {
        return $this->canExport ?? true;
    }

    /**
     * Whether there should be post type archives,
     * or if a string, the archive slug to use.
     *
     * @return bool
     */
    public function getHasArchive(): bool
    {
        return $this->hasArchive ?? true;
    }

    /**
     * Whether to exclude posts with this post type
     * from front end search results.
     *
     * @return bool
     */
    public function getExcludeFromSearch(): bool
    {
        return $this->excludeFromSearch ?? false;
    }

    /**
     * Whether queries can be performed on
     * the front end for the post type.
     *
     * @return bool
     */
    public function getPubliclyQueryable(): bool
    {
        return $this->publiclyQueryable ?? true;
    }

    /**
     * The string to use to build the read, edit, and delete capabilities.
     *
     * @return string
     */
    public function getCapabilityType(): string
    {
        return $this->capabilityType ?? 'post';
    }

    /**
     * An array of the capabilities for this post type.
     *
     * @return array
     */
    public function getCapabilities(): array
    {
        return $this->capabilities ?? [];
    }

    /**
     * Whether to include the post type in the REST API.
     *
     * @return bool
     */
    public function getShowInRest(): bool
    {
        return $this->showInRest ?? true;
    }

    /**
     * The base slug that this post type will use when accessed using the REST API.
     *
     * @return string|null
     */
    public function getRestBase(): ?string
    {
        return $this->restBase ?? null;
    }

    /**
     * Triggers the handling of rewrites for this post type.
     *
     * @return array
     */
    public function getRewrite(): array
    {
        return $this->rewrite ?? [];
    }

    /**
     * Whether to delete posts of this type when deleting a user.
     *
     * @return bool|null
     */
    public function getDeleteWithUser(): ?bool
    {
        return $this->deleteWithUser ?? null;
    }
}
