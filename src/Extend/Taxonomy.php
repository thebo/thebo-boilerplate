<?php

namespace KobaltDigital\Extend;

class Taxonomy
{
    /**
     * Taxonomy must not exceed 32 characters.
     *
     * @return string
     */
    public function getTaxonomy(): string
    {
        return $this->taxonomy ?? '';
    }

    /**
     * Post type or array of post types with which the taxonomy should be associated.
     *
     * @return string|array
     */
    public function getPostType()
    {
        return $this->postType ?? '';
    }

    /**
     * A short descriptive summary of what the taxonomy is.
     *
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description ?? '';
    }

    /**
     * An array of labels for this taxonomy.
     *
     * @return array
     */
    public function getLabels(): ?array
    {
        return $this->labels ?? null;
    }

    /**
     * Whether the taxonomy is hierarchical.
     *
     * @return bool
     */
    public function getHierarchical(): bool
    {
        return $this->hierarchical ?? false;
    }

    /**
     * Whether a taxonomy is intended for use publicly either
     * via the admin interface or by front-end users
     *
     * @return bool
     */
    public function getPublic(): bool
    {
        return $this->public ?? true;
    }

    /**
     * Whether to generate and allow a UI for managing
     * terms in this taxonomy in the admin.
     *
     * @return bool
     */
    public function getShowUi(): bool
    {
        return $this->showUi ?? true;
    }

    /**
     * Whether to show the taxonomy in the admin menu.
     * If true, the taxonomy is shown as a submenu of the post type menu.
     *
     * @return bool
     */
    public function getShowInMenu(): bool
    {
        return $this->showInMenu ?? true;
    }

    /**
     * Makes this taxonomy available for
     * selection in navigation menus.
     *
     * @return bool
     */
    public function getShowInNavMenus(): bool
    {
        return $this->showInNavMenus ?? true;
    }

    /**
     * Whether the taxonomy is publicly queryable.
     *
     * @return bool
     */
    public function getPubliclyQueryable(): bool
    {
        return $this->publiclyQueryable ?? true;
    }

    /**
     * Array of capabilities for this taxonomy.
     *
     * @return array
     */
    public function getCapabilities(): array
    {
        return $this->capabilities ?? [];
    }

    /**
     * Whether to include the taxonomy in the REST API.
     *
     * @return bool
     */
    public function getShowInRest(): bool
    {
        return $this->showInRest ?? true;
    }

    /**
     * To change the base url of REST API route.
     *
     * @return string|null
     */
    public function getRestBase(): ?string
    {
        return $this->restBase ?? null;
    }

    /**
     * Triggers the handling of rewrites for this taxonomy.
     *
     * @return array
     */
    public function getRewrite(): array
    {
        return $this->rewrite ?? [];
    }
}
