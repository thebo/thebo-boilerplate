<?php

namespace KobaltDigital\Extend;

class OptionsPage
{
    /**
     * The page title that will be shown at the top of the page.
     *
     * @return string
     */
    public function getPageTitle(): string
    {
        return $this->pageTitle ?? '';
    }

    /**
     * The menu title that will be shown in the admin menu.
     *
     * @return string
     */
    public function getMenuTitle(): string
    {
        return $this->menuTitle ?? '';
    }


    /**
     * A unique slug that will be used to identify this options page.
     *
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug ?? '';
    }

    /**
     * The slug of the parent page, if null this will be a top level page.
     *
     * @return null|string
     */
    public function getParent(): ?string
    {
        return $this->parent ?? null;
    }

    /**
     * The capability required to access this options page.
     *
     * @return string
     */
    public function getCapability(): string
    {
        return $this->capability ?? '';
    }

    /**
     * Determines whether the user is redirected to the first child page.
     *
     * @return bool
     */
    public function getRedirect(): bool
    {
        return $this->redirect ?? false;
    }
}
