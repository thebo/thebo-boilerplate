<?php

namespace KobaltDigital\Extend;

class Filter
{
    /**
     * The name (or array) of the filter(s) to add the callback to.
     *
     * @return string|array
     */
    public function getFilter()
    {
        return $this->filter ?? '';
    }

    /**
     * Used to specify the order in which the functions associated with a particular filter
     * are executed. Lower numbers correspond with earlier execution, and functions with the
     * same priority are executed in the order in which they were added to the filter.
     *
     * @return int
     */
    public function getPriority(): int
    {
        return $this->priority ?? 10;
    }
}
