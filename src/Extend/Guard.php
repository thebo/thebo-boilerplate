<?php

namespace KobaltDigital\Extend;

class Guard
{
    /**
     * Guard name to be replaced in blade files.
     *
     * @return string
     */
    public function getGuard(): string
    {
        return $this->guard ?? '';
    }
}
