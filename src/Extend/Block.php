<?php

namespace KobaltDigital\Extend;

use mysql_xdevapi\Exception;
use Illuminate\View\ComponentAttributeBag;

class Block
{
    /**
     * A unique name that identifies the block (without namespace). For example ‘testimonial’.
     *
     * Note: A block name can only contain lowercase alphanumeric characters and dashes,
     * and must begin with a letter.
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name ?? '';
    }

    /**
     * The display title for your block. For example ‘Testimonial’.
     *
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title ?? '';
    }

    /**
     * This is a short description for your block.
     *
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description ?? '';
    }

    /**
     * Blocks are grouped into categories to help users browse and discover them.
     * The core provided categories are [ common | formatting | layout | widgets | embed ].
     *
     * @return string
     */
    public function getCategory(): string
    {
        return $this->category ?? '';
    }

    /**
     * An icon property can be specified to make it easier to identify a block.
     * These can be any of WordPress’ Dashicons, or a custom svg element.
     *
     * @return string|array
     */
    public function getIcon()
    {
        return $this->icon ?? '';
    }

    /**
     * The default block alignment. Available settings are
     * “left”, “center”, “right”, “wide” and “full”.
     * Defaults to an empty string.
     *
     * @return string
     */
    public function getAlign(): string
    {
        return $this->align ?? '';
    }

    /**
     * The display mode for your block. Available settings are “auto”, “preview” and “edit”.
     * Defaults to “preview”.
     *
     * auto: Preview is shown by default but changes to edit form when block is selected.
     * preview: Preview is always shown. Edit form appears in sidebar when block is selected.
     * edit: Edit form is always shown.
     *
     * @return string
     */
    public function getMode(): string
    {
        return $this->mode ?? '';
    }

    /**
     * An array of search terms to help user discover the block while searching.
     *
     * @return array
     */
    public function getKeywords(): array
    {
        return $this->keywords ?? [];
    }

    /**
     * An array of post types to restrict this block type to.
     *
     * @return array
     */
    public function getPostTypes(): array
    {
        return $this->post_types ?? [];
    }

    /**
     * An array of features to support.
     *
     * @return array
     */
    public function getSupports(): array
    {
        return $this->supports ?? [];
    }

    /**
     * The path to a template file used to render the block HTML.
     *
     * @return string
     */
    public function getTemplate(): string
    {
        return $this->template ?? '';
    }

    /**
     * Callback function to render the block.
     *
     * @param array $block The block settings and attributes.
     * @param string $content The block inner HTML (empty).
     * @param bool $is_preview True during AJAX preview.
     * @param (int|string) $post_id The post ID this block is saved to.
     */
    public function handle($block, $content, $is_preview, $post_id): void
    {
        if (method_exists($this, 'render')) {
            $view = $this->render();
        } elseif (property_exists($this, 'template')) {
            $view = view($this->getTemplate());
        } else {
            throw new Exception('template or render method of block not defined');
        }

        echo $view->with([
            'attributes' => new ComponentAttributeBag([
                'class' => $block['className'] ?? '',
                'id' => $block['id'],
            ]),
            'content' => $content,
            'block' => $block,
            'is_preview' => $is_preview,
            'post_id' => $post_id,
        ]);
    }
}
