<?php

namespace KobaltDigital\Extend;

class Action
{
    /**
     * The name (or array) of the action(s) to add the callback to
     *
     * @return string|array
     */
    public function getAction()
    {
        return $this->action ?? '';
    }

    /**
     * Used to specify the order in which the functions associated with a particular action
     * are executed. Lower numbers correspond with earlier execution, and functions with the
     * same priority are executed in the order in which they were added to the action.
     *
     * @return int
     */
    public function getPriority(): int
    {
        return $this->priority ?? 10;
    }
}
