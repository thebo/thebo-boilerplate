<?php

namespace KobaltDigital\Extend;

class Sidebar
{
    /**
     * The name or title of the sidebar displayed in the Widgets interface.
     *
     * @return string
     */
    public function getSidebar(): string
    {
        return $this->sidebar ?? '';
    }


    /**
     * The unique identifier by which the sidebar will be called.
     *
     * @return string
     */
    public function getId(): string
    {
        return $this->id ?? '';
    }

    /**
     * Description of the sidebar, displayed in the Widgets interface.
     *
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description ?? '';
    }

    /**
     * Extra CSS class to assign to the sidebar in the Widgets interface.
     *
     * @return string
     */
    public function getClass(): string
    {
        return $this->class ?? '';
    }

    /**
     * HTML content to prepend to each widget's HTML output when assigned to this sidebar.
     * Receives the widget's ID attribute as %1$s and class name as %2$s.
     *
     * @return string
     */
    public function getBeforeWidget(): string
    {
        return $this->beforeWidget ?? '';
    }

    /**
     * HTML content to append to each widget's HTML output when assigned to this sidebar.
     *
     * @return string
     */
    public function getAfterWidget(): string
    {
        return $this->afterWidget ?? '';
    }

    /**
     * HTML content to prepend to the sidebar title when displayed.
     *
     * @return string
     */
    public function getBeforeTitle(): string
    {
        return $this->beforeTitle ?? '';
    }

    /**
     * HTML content to append to the sidebar title when displayed.
     *
     * @return string
     */
    public function getAfterTitle(): string
    {
        return $this->afterTitle ?? '';
    }

    /**
     * HTML content to prepend to the sidebar when displayed.
     * Receives the $id argument as %1$s and $class as %2$s.
     *
     * @return string
     */
    public function getBeforeSidebar(): string
    {
        return $this->beforeSidebar ?? '';
    }

    /**
     * HTML content to append to the sidebar when displayed.
     *
     * @return string
     */
    public function getAfterSidebar(): string
    {
        return $this->afterSidebar ?? '';
    }

    /**
     * Whether to include the sidebar in the REST API.
     *
     * @return bool
     */
    public function getShowInRest(): bool
    {
        return $this->showInRest ?? true;
    }
}
