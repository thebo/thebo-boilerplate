<?php

namespace KobaltDigital\Filters;

use Illuminate\View\View;
use KobaltDigital\Extend\Filter;

class RenderSearchFormWhenGetSearchFormIsApplied extends Filter
{
    /**
     * The name (or array) of the filter(s) to add the callback to.
     *
     * @var string|array
     */
    protected $filter = 'get_search_form';

    /**
     * Used to specify the order in which the functions associated with a particular filter
     * are executed. Lower numbers correspond with earlier execution, and functions with the
     * same priority are executed in the order in which they were added to the filter.
     *
     * @var int
     */
    protected int $priority = 9999;

    /**
    * Filters the HTML output of the search form.
    *
    * @param string $form The search form HTML output.
    * @param array $args The array of arguments for building the search form.
    *                     See get_search_form() for information on accepted arguments.
    * @since 2.7.0
    * @since 5.5.0 The `$args` parameter was added.
    *
    */
    public function handle(string $form, array $args): View
    {
        return view('searchform');
    }
}
