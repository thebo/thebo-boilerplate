<?php

namespace KobaltDigital\Filters\Register;

use Illuminate\Support\Str;
use KobaltDigital\Extend\Filter;

class Templates extends Filter
{
    /**
     * The name of the filter to add the callback to.
     *
     * @var string
     */
    protected string $filter = 'theme_page_templates';

    /**
     * Used to specify the order in which the functions associated with a particular filter
     * are executed. Lower numbers correspond with earlier execution, and functions with the
     * same priority are executed in the order in which they were added to the filter.
     *
     * @var int
     */
    protected int $priority = 20;

    /**
     * The callback to be run when the filter is applied.
     *
     * @param array $page_templates
     * @param $theme
     * @param $post
     * @return array
     */
    public function handle(array $page_templates, $theme, $post): array
    {
        collect(config('blade.templates'))->each(function ($directory) use (&$page_templates) {
            collect(getViewsInDirectory($directory))->each(function ($view) use (&$page_templates, $directory) {
                $file = sprintf('%s%s.blade.php', $directory, $view);
                $file = file_get_contents($file);
                preg_match("/\{\{--\s?Template\s?(Name)?:\s?(.*)\s?--\}\}/", $file, $match);

                if (!isset($match[2])) {
                    return;
                }

                $page_templates[$view] = trim($match[2]);
            });
        });

        return $page_templates;
    }
}
