<?php

namespace KobaltDigital\Filters\Woocommerce;

use Illuminate\Support\Str;
use KobaltDigital\Extend\Filter;
use Illuminate\Support\Collection;
use Illuminate\View\ComponentAttributeBag;

class RenderWoocommerceTemplate extends Filter
{
    /**
     * The name (or array) of the filter(s) to add the callback to.
     *
     * @var string|array
     */
    protected $filter = 'wc_get_template';

    /**
     * Used to specify the order in which the functions associated with a particular filter
     * are executed. Lower numbers correspond with earlier execution, and functions with the
     * same priority are executed in the order in which they were added to the filter.
     *
     * @var int
     */
    protected int $priority = 10;

    /**
     * Used to store all the available woocommerce views
     *
     * @var Collection
     */
    protected Collection $views;

    public function __construct()
    {
        $views = collect(
            getFilesInDirectory(
                get_template_directory() . '/resources/views/woocommerce/',
                '.blade.php'
            )
        );

        $this->views = $views->map(function ($view) {
            $view = str_replace('.blade.php', '', $view);
            $view = Str::afterLast($view, 'resources/views/woocommerce/');
            return 'woocommerce.' . Str::replace('/', '.', $view);
        });
    }


    /**
     * @param $template
     * @param $template_name
     * @param $args
     * @param $template_path
     * @param $default_path
     * @return string
     */
    public function handle($template, $template_name, $args, $template_path, $default_path): string
    {
        $name = Str::replace('/', '.', $template_name);
        $name = Str::replace('.php', '', $name);
        $name = 'woocommerce.' . $name;

        if ($this->views->contains($name)) {
            echo view($name, array_merge([
                'attributes' => new ComponentAttributeBag($args),
            ], $args));

            return 'php://temp';
        }

        return $template;
    }
}
