<?php

namespace KobaltDigital\Filters;

use Illuminate\Support\Str;
use KobaltDigital\Extend\Filter;
use Illuminate\View\ComponentAttributeBag;

class RenderTemplate extends Filter
{
    /**
     * The name (or array) of the filter(s) to add the callback to.
     *
     * @var string
     */
    protected string $filter = 'template_include';

    /**
     * Used to specify the order in which the functions associated with a particular filter
     * are executed. Lower numbers correspond with earlier execution, and functions with the
     * same priority are executed in the order in which they were added to the filter.
     *
     * @var int
     */
    protected int $priority = 999999;

    /**
     * The callback to be run when the filter is applied.
     *
     * @param string $view
     * @return string
     */
    public function handle(string $view): string
    {
        global $template;

        if ($this->isThemeTemplate($view)) {
            include($view);
            return 'php://temp';
        }

        if ($view === 'php://temp') {
            return $view;
        }

        if (!Str::endsWith($view, '.blade.php')) {
            $template = $view;

            if (!config('blade.default_layout')) {
                return $view;
            }

            ob_start();
            include($view);
            $slot = ob_get_contents();
            ob_end_clean();

            $view = view(config('blade.default_layout'), [
              'slot' => $slot,
              'attributes' => new ComponentAttributeBag([])
            ]);
        } else {
            $view = view(Str::remove('.blade.php', $view));
            $template = $view->getPath();
        }

        echo $view;
        return 'php://temp';
    }

    private function isThemeTemplate(string $view)
    {
        return Str::contains($view, sprintf('themes/%s', config('theme.name')));
    }
}
