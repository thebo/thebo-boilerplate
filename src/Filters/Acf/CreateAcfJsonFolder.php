<?php

namespace KobaltDigital\Acf\Filters;

use KobaltDigital\Extend\Filter;

class CreateAcfJsonFolder extends Filter
{
    /**
     * The name (or array) of the filter(s) to add the callback to.
     *
     * @var string|array
     */
    protected $filter = 'acf/settings/save_json';

    /**
     * Used to specify the order in which the functions associated with a particular filter
     * are executed. Lower numbers correspond with earlier execution, and functions with the
     * same priority are executed in the order in which they were added to the filter.
     *
     * @var int
     */
    protected int $priority = 10;

    /**
     * The callback to be run when the filter is applied.
     *
     * @param $path
     * @return string
     */
    public function handle($path): string
    {
        $dir = get_stylesheet_directory() . '/acf-json';

        if (is_dir($dir)) {
            return $path;
        }

        if (!mkdir($dir) && !is_dir($dir)) {
            throw new \RuntimeException(sprintf('Directory "%s" was not created', $dir));
        }

        return $path;
    }
}
