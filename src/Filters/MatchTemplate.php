<?php

namespace KobaltDigital\Filters;

use Illuminate\Support\Str;
use KobaltDigital\Extend\Filter;
use Illuminate\Support\Collection;

class MatchTemplate extends Filter
{

    /**
     * The name of the filter to add the callback to.
     *
     * @var string
     */
    protected $filter = [
        '404_template',
        'page_template',
        'archive_template',
        'attachment_template',
        'author_template',
        'category_template',
        'date_template',
        'embed_template',
        'frontpage_template',
        'home_template',
        'index_template',
        'page_template',
        'paged_template',
        'privacypolicy_template',
        'search_template',
        'single_template',
        'singular_template',
        'tag_template',
        'taxonomy_template',
    ];

    /**
     * Used to specify the order in which the functions associated with a particular filter
     * are executed. Lower numbers correspond with earlier execution, and functions with the
     * same priority are executed in the order in which they were added to the filter.
     *
     * @var int
     */
    protected int $priority = 20;

    protected Collection $views;

    public function __construct()
    {
        $this->views = collect(config('blade.templates'))->flatMap(function ($dir) {
            return getViewsInDirectory($dir);
        });
    }

    /**
     * The callback to be run when the filter is applied.
     *
     * @param string $template
     * @param string $type
     * @param string[] $templates
     * @return string
     */
    public function handle(string $template, string $type, array $templates): string
    {
        $filename = Str::afterLast($template, '/');

        // If WordPress found a matching .php file, bail.
        if ($template && $filename !== 'index.php') {
            return $template;
        }

        // If this is the index.php and the filesize is bigger than 28kb, bail
        if ($filename === 'index.php' && filesize($template) > 28) {
            return $template;
        }

        // Loop though the given templates, strip the extension and get the first view that exists.
        $view = collect($templates)->map(function ($template) {
            return str_replace('.php', '', $template);
        })->first(function ($template) {
            return $this->views->contains($template);
        });

        // If there is a template found, echo it and return a temp php file so WordPress stops searching.
        if ($view) {
            return $view . '.blade.php';
        }

        return false;
    }
}
