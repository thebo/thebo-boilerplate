<?php
namespace KobaltDigital;

class RegisterFilters
{
    public function __construct()
    {
        $filters = array_merge(
            remember('user/filters', function () {
                return getClassesInDirectory(get_theme_file_path('App/Filters/'), config('theme.namespace') . 'Filters');
            }),
            remember('default/filters', function () {
                return getClassesInDirectory(__DIR__ . '/Filters', 'KobaltDigital\Filters');
            })
        );

        collect($filters)->each(function ($class, $key) {
            if (!$class) {
                return;
            }

            if (!class_exists($class)) {
                return;
            }

            $filter = new $class();

            if (!$filter->getFilter()) {
                return;
            }

            $filters = is_array($filter->getFilter()) ? $filter->getFilter() : [$filter->getFilter()];

            $reflector = new \ReflectionClass($class);
            $accepted_args = count($reflector->getMethod('handle')->getParameters());

            collect($filters)->each(function ($name) use ($filter, $accepted_args) {
                add_filter($name, [$filter, 'handle'], $filter->getPriority(), $accepted_args);
            });
        });
    }
}
